**Sympl** is a series of scripts and magic which sit on top of the reliable
[Debian Linux](https://www.debian.org/), configuring your server automatically, 
based on simple settings on the server's filesystem.

It provides:

* [Apache](https://www.apache.org/) web hosting.
* [Let's Encrypt](https://letsencrypt.org/) provided free certificates.
* [PHP](https://www.php.net/) 8.2 by default, with selectable PHP versions from 5.6 to 8.2 from [deb.sury.org](https://deb.sury.org/).
* [MariaDB](https://mariadb.org/) (MySQL) database server.
* [phpMyAdmin](https://www.phpmyadmin.net/) control panel for your databases.
* [Pure-FTPd](https://www.pureftpd.org/project/pure-ftpd/) FTP server.
* [SpamAssassin](https://spamassassin.apache.org/) spam filtering.
* [ClamAV](https://www.clamav.net/) email anti-virus.
* [Dovecot](https://www.dovecot.org/) IMAP and POP3 mailboxes.
* [Exim](https://www.exim.org/) email transport.
* [Roundcube](https://roundcube.net/) webmail.
* Automatic local monitoring.

... with security updates from the Debian project!

Sympl is built on [Symbiosis](https://github.com/BytemarkHosting/symbiosis) and
years of System Administration support and best practices, meaning you spend
less time getting things up and running.
