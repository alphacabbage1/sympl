sympl-core (12.20240507.0) stable; urgency=medium

  * Fix initial sympl-ssl set selection. Thanks to @techwilk for the report and fix.

 -- Paul Cammish <sympl@kelduum.net>  Tue, 07 May 2024 09:37:00 +0100
 
sympl-core (12.20240419.0) stable; urgency=medium

  * Added 'sympl version' to output a list of package versions

 -- Paul Cammish <sympl@kelduum.net>  Fri, 19 Apr 2024 12:43:00 +0100
 
sympl-core (12.20240319.0) stable; urgency=medium

  * Updated sympl-filesystem-security for better composer compatibility
  * Added dbus dependency

 -- Paul Cammish <sympl@kelduum.net>  Tue, 19 Mar 2024 16:37:00 +0000
 
sympl-core (12.20230612.0) stable; urgency=medium

  * Added rsyslog dependency so logs are written to /var/log as per previous Debian
  * Moved firewall SSH patterns to core

 -- Paul Cammish <sympl@kelduum.net>  Mon, 12 Jun 2023 11:00:00 +0100
 
sympl-core (12.20230609.0) stable; urgency=medium

  * Debian Bookworm release
  * Automatically populate /etc/hosts on install
  * Include git package as as dependency for future functionality
  * sympl-filesystem-security - Don't change permissions on sympl/php or sympl/monit.d
  * Reset ssl-provider default domain if it was changed to 'selfsigned'
  * Added basic usage reporting. Touch /etc/sympl/disable-usage-reporting to disable

 -- Paul Cammish <sympl@kelduum.net>  Fri, 09 Jun 2023 16:00:00 +0000
 
sympl-core (12.20230530.0) stable; urgency=medium

  * sympl-filesystem-security - Don't change permissions on sympl/php

 -- Paul Cammish <sympl@kelduum.net>  Wed, 31 May 2023 10:30:00 +0100

sympl-core (12.20230526.0) stable; urgency=medium

  * Automatically populate /etc/hosts on install

 -- Paul Cammish <sympl@kelduum.net>  Tue, 02 May 2023 16:11:00 +0000

sympl-core (12.20230430.0) stable; urgency=medium

  * Version 12 for Debian Bookworm

 -- Paul Cammish <sympl@kelduum.net>  Sun, 30 Apr 2023 21:25:00 +0000

sympl-core (11.20220426.0) stable; urgency=medium

  * Fix control logic in sympl-filesystem-security

 -- Paul Cammish <sympl@kelduum.net>  Tue, 26 Apr 2022 10:00:00 +0100

sympl-core (11.20220323.0) stable; urgency=medium

  * Replace tempfile with mktemp in sympl-generate-dhparams

 -- Paul Cammish <sympl@kelduum.net>  Tue, 22 Mar 2022 14:00:00 +0000
 
sympl-core (11.20220322.1) stable; urgency=low

  * Update references from sympl.host to sympl.io

 -- Paul Cammish <sympl@kelduum.net>  Tue, 22 Mar 2022 14:00:00 +0000
 
sympl-core (11.20211213.2) stable; urgency=medium

  * Fix for MOTD installation

 -- Paul Cammish <sympl@kelduum.net>  Mon, 13 Dec 2021 17:02:00 +0000
 
sympl-core (11.20211213.1) stable; urgency=medium

  * Handle other case in workaround

 -- Paul Cammish <sympl@kelduum.net>  Mon, 13 Dec 2021 13:48:00 +0000
 
sympl-core (11.20211213.0) stable; urgency=medium

  * Updated workaround for Let's Encrypt cross-signed intermediate

 -- Paul Cammish <sympl@kelduum.net>  Mon, 13 Dec 2021 10:23:00 +0000
 
sympl-core (11.20211003.0) stable; urgency=medium

  * Workaround for Let's Encrypt cross-signed intermediate

 -- Paul Cammish <sympl@kelduum.net>  Sun, 03 Oct 2021 12:27:00 +0100

sympl-core (11.20211001.0) stable; urgency=medium

  * Updated acme-client library to 2.0.9

 -- Paul Cammish <sympl@kelduum.net>  Fri, 01 Oct 2021 19:15:00 +0100

sympl-core (11.20210819.0) stable; urgency=medium

  * Fixes for missing MOTD

 -- Paul Cammish <sympl@kelduum.net>  Thu, 19 Aug 2021 10:30:00 +0100

sympl-core (11.20210818.1) stable; urgency=medium

  * Debian Bullseye Release

 -- Paul Cammish <sympl@kelduum.net>  Wed, 18 Aug 2021 14:00:00 +0100
 
sympl-core (11.20210818.0) stable; urgency=medium

  * Check htdocs/stats for AWFFull rather than Webalizer

 -- Paul Cammish <sympl@kelduum.net>  Wed, 18 Aug 2021 12:15:00 +0100
 
sympl-core (11.20210409.0) stable; urgency=medium
 
  * Updated sympl.host to sympl.io
  * Updated automated testing to remove broken tests

 -- Paul Cammish <sympl@kelduum.net>  Fri, 09 Apr 2021 10:01:12 +0100

sympl-core (11.20210215.0) stable; urgency=medium

  * Updated version numbering format

 -- Paul Cammish <sympl@kelduum.net>  Mon, 15 Feb 2021 12:00:00 +0000
 
sympl-core (11.210209.0) stable; urgency=medium

  * Updated version for Debian Bullseye
  * Tidied up old dependencies
  * Updated MOTD version number

 -- Paul Cammish <sympl@kelduum.net>  Tue, 09 Feb 2021 14:56:02 +0000
 
sympl-core (10.0.200923.0) stable; urgency=medium
 
  * Properly filter public/cgi-bin

 -- Paul Cammish <sympl@kelduum.net>  Wed, 23 Sep 2020 13:50:11 +0100
 
sympl-core (10.0.200915.0) stable; urgency=medium
 
  * Added Debian Backports with configuration to enable phpmyadmin installation

 -- Paul Cammish <sympl@kelduum.net>  Tue, 15 Sep 2020 16:45:10 +0100
 
sympl-core (10.0.200909.0) stable; urgency=medium
 
  * sympl-filesystem-security: don't overwite permission in public/cgi-bin (#299)
  * sympl-filesystem-security: correctly read the group id (#298)
  * sympl-cli: fix permissions on newly created domains (#295)

 -- Paul Cammish <sympl@kelduum.net>  Wed, 09 Sep 2020 12:22:09 +0100

sympl-core (10.0.200512.0) stable; urgency=low
 
  * Added functionality to the sympl cli for FTP user management

 -- Doug Targett <dougtargett@gmail.com>  Tue, 05 May 2020 17:55:30 +0100

sympl-core (10.0.200510.0) stable; urgency=medium

  * Remove debug output from sympl-filesystem-security

 -- Paul Cammish <sympl@kelduum.net>  Sun, 10 May 2020 15:06:08 +0100

sympl-core (10.0.200427.0) stable; urgency=medium

  * Further fixes to prevent sympl-filesystem-security from changing permissions where it shouldn't. (#290)

 -- Paul Cammish <sympl@kelduum.net>  Mon, 27 Apr 2020 13:47:07 +0100

sympl-core (10.0.200420.0) stable; urgency=medium

  * Prevent sympl-filesystem-security from changing permissions of /etc/firewall/local.d/ contents.

 -- Paul Cammish <sympl@kelduum.net>  Mon, 20 Apr 2020 15:15:06 +0100

sympl-core (10.0.200415.0) stable; urgency=medium

  * Added --verbose switch to sympl-filesystem-security
  * Fixed issue #280 with sympl-filesystem-security
  
 -- Paul Cammish <sympl@kelduum.net>  Wed, 15 Apr 2020 15:18:05 +0100
 
sympl-core (10.0.191231.0) stable; urgency=medium

  * Fixed inconsistency with disable-filesystem-security switches.

 -- Paul Cammish <sympl@kelduum.net>  Tue, 31 Dec 2019 08:19:04 +0000

sympl-core (10.0.191216.0) stable; urgency=medium

  * Add sympl user to relevant groups on each install.

 -- Paul Cammish <sympl@kelduum.net>  Mon, 16 Dec 2019 12:22:03 +0000

sympl-core (10.0.191205.0) stable; urgency=medium

  * Updated IPv6 Only workaround.

 -- Paul Cammish <sympl@kelduum.net>  Thu, 05 Dec 2019 15:43:02 +0000
 
sympl-core (10.0.191017.0) stable; urgency=medium

  * Updated sympl-ssl to use Let's Encrypt ACME v02 API

 -- Paul Cammish <sympl@kelduum.net>  Thu, 17 Oct 2019 13:45:01 +0100

sympl-core (10.0.190908.0) stable; urgency=medium

  * Set default threshold for LE cert renewal to 30 days.

 -- Paul Cammish <sympl@kelduum.net>  Sun, 08 Sep 2019 12:03:00 +0100

sympl-core (10.0.190816.0) stable; urgency=medium

  * Adds detection of NAT64 environments for sympl-ssl wrapper.

 -- Paul Cammish <sympl@kelduum.net>  Fri, 16 Aug 2019 18:11:00 +0100

sympl-core (10.0.190729.0) stable; urgency=medium

  * Copy root users authorized_keys to Sympl user on first install.

 -- Paul Cammish <sympl@kelduum.net>  Mon, 29 Jul 2019 12:49:00 +0100

sympl-core (10.0.190725.0) stable; urgency=medium

  * Fixed typo in sympl CLI

 -- Paul Cammish <sympl@kelduum.net>  Thu, 25 Jul 2019 23:53:00 +0100

sympl-core (10.0.190705.1) stable; urgency=medium

  * Removed beta flag from MOTD

 -- Paul Cammish <sympl@kelduum.net>  Fri, 05 Jul 2019 13:04:00 +0100

sympl-core (10.0.190705.0) stable; urgency=medium

  * Updated 'sympl' parser, added 'sympl update' function.

 -- Paul Cammish <sympl@kelduum.net>  Fri, 05 Jul 2019 13:01:00 +0100

sympl-core (10.0.190704.0) stable; urgency=medium

  * Workaround for sympl-ssl bug #249 under IPv6 only.

 -- Paul Cammish <sympl@kelduum.net>  Thu, 04 Jul 2019 16:10:00 +0100

sympl-core (10.0.190702.2) stable; urgency=medium

  * Removed mailbox permission rewriting

 -- Paul Cammish <sympl@kelduum.net>  Tue, 02 Jul 2019 18:49:42 +0100

sympl-core (10.0.190702.1) stable; urgency=medium

  * Disabled hostname enforcement

 -- Paul Cammish <sympl@kelduum.net>  Tue, 02 Jul 2019 17:00:58 +0100

sympl-core (10.0.190702.0) stable; urgency=medium

  * Adjusted security permissions for domains Exim config files

 -- Paul Cammish <sympl@kelduum.net>  Tue, 02 Jul 2019 12:13:58 +0100

sympl-core (10.0.190628.0) stable; urgency=medium

  * Adjusted permissions for config/dkim

 -- Paul Cammish <sympl@kelduum.net>  Fri, 28 Jun 2019 16:25:19 +0100

sympl-core (10.0.190625.0) stable; urgency=medium

  * First update for sympl command line
  * Fixed edge case in sympl-filesystem-security

 -- Paul Cammish <sympl@kelduum.net>  Tue, 25 Jun 2019 15:46:00 +0100

sympl-core (10.0.190624.0) stable; urgency=medium

  * Adjusted MOTD Banner
  * Updated sympl-filesystem-security with tweaks to paths/logic

 -- Paul Cammish <sympl@kelduum.net>  Mon, 24 Jun 2019 10:27:00 +0100

sympl-core (10.0.190621.0) stable; urgency=medium

  * Created Sympl v10.0 (Debian Buster)

 -- Paul Cammish <sympl@kelduum.net>  Fri, 21 Jun 2019 10:15:00 +0100

sympl-core (9.0.190621.0) stable; urgency=medium

  * Moved sympl-ssl to sbin to avoid permissions/hook issues.

 -- Paul Cammish <sympl@kelduum.net>  Fri, 21 Jun 2019 09:19:00 +0100

sympl-core (9.0.190620.1) stable; urgency=medium

  * Updated recommended packages
  * Updated MOTD banner

 -- Paul Cammish <sympl@kelduum.net>  Thu, 20 Jun 2019 14:31:00 +0100

sympl-core (9.0.190620.0) stable; urgency=medium

  * Added cron for sympl-ssl, making sure it runs first

 -- Paul Cammish <sympl@kelduum.net>  Thu, 20 Jun 2019 12:12:00 +0100

sympl-core (9.0.190619.0) stable; urgency=medium

  * Removed backward compatibility for /etc/symbiosis

 -- Paul Cammish <sympl@kelduum.net>  Wed, 19 Jun 2019 17:12:00 +0100

sympl-core (9.0.190614.0) stable; urgency=medium

  * Added stubbed out version of the sympl command line.

 -- Paul Cammish <sympl@kelduum.net>  Fri, 14 Jun 2019 09:57:00 +0100

sympl-core (9.0.190613.0) stable; urgency=medium

  * Improved hostname handling on install

 -- Paul Cammish <sympl@kelduum.net>  Thu, 13 Jun 2019 15:52:00 +0100

sympl-core (9.0.190612.0) stable; urgency=medium

  * Improved security for /srv

 -- Paul Cammish <sympl@kelduum.net>  Wed, 12 Jun 2019 14:23:00 +0100

sympl-core (9.0.190611.0) stable; urgency=medium

  * Merged sympl-common into sympl-core

 -- Paul Cammish <sympl@kelduum.net>  Tue, 11 Jun 2019 13:18:00 +0100

sympl-core (9.0.190609.0) stable; urgency=medium

  * Added Beta tag to MOTDs
  * Added recommends for common apps: htop, vim, nano, wget, curl

 -- Paul Cammish <sympl@kelduum.net>  Sun, 09 Jun 2019 22:56:00 +0100
 
sympl-core (9.0.190604.0) stable; urgency=medium

  * Replaced banners
  * Replaced mentions of Symbiosis with Sympl.
  * Renamed package to sympl-core

 -- Paul Cammish <sympl@kelduum.net>  Tue, 04 Jun 2019 23:00:00 +0100

bytemark-symbiosis (2017:0818) stable; urgency=medium

  * Added systemd unit to output Symbiosis logo on boot.

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Fri, 18 Aug 2017 18:17:12 +0100

bytemark-symbiosis (2017:0424) stable; urgency=medium

  * Added tentacles to Bytemark Symbiosis logo in honour of Debian Stretch.
  * Updated copyright file.

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Mon, 24 Apr 2017 16:18:57 +0100

bytemark-symbiosis (2015:0825) stable; urgency=medium

  * Now depends on bytemark-keys as an alternative to symbiosis-keys.
  * General dynamic MOTD reformatting.
  * Updated copyright.

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Tue, 25 Aug 2015 14:18:30 +0100

bytemark-symbiosis (2015:0618) stable; urgency=medium

  * Now depends on bytemark-key instead of symbiosis-key.
  * New boot splash.
  * Dynamic MOTD added to alert user to resource usage issues.
  * Updated maintainers + uploaders 

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Wed, 17 Jun 2015 10:54:13 +0100

bytemark-symbiosis (2015:0128) stable; urgency=low

  * Updated the Debian standards.

 -- Steve Kemp <steve@bytemark.co.uk>  Wed, 28 Jan 2015 09:55:09 +0000

bytemark-symbiosis (2014:0218) stable; urgency=low

  * Updated init script to start after SSH.
  * Added lsb-base dependency.
  * General init script tidy-up, tinker, and fix.

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Tue, 18 Feb 2014 14:34:38 +0000

bytemark-symbiosis (2014:0214) stable; urgency=low

  * Added bug control.
  * Added recommended use of XMPP pacakge.

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Mon, 17 Feb 2014 10:16:03 +0000

bytemark-symbiosis (2014:0113) stable; urgency=low

  * Moved pam package into this source tree.
  * Changed maintainer and uploaders.

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Mon, 13 Jan 2014 16:03:27 +0000

bytemark-symbiosis (2013:0913) stable; urgency=low

  * Updated init script.  To be amazing.

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Fri, 13 Sep 2013 16:35:14 +0100

bytemark-symbiosis (2012:1109) stable; urgency=low

  * Added openssh-server as a recommendation.

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Fri, 09 Nov 2012 16:14:53 +0000

bytemark-symbiosis (2012:0607) stable; urgency=low

  * Updated dependencies to add in symbiosis-pam.
  * Removed use of cdbs.

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Thu, 07 Jun 2012 17:28:08 +0100

bytemark-symbiosis (2012:0222) stable; urgency=low

  * Updated versioned dependencies to use the stable packages.
  * Updated copyright dates.
  * Updated postinst not to repeatedly add to motd.tail

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Wed, 22 Feb 2012 20:40:28 +0000

bytemark-symbiosis (2012:0208) stable; urgency=low

  * Updated versioned dependencies to use the RC3 packages, i.e. 2012:0208.

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Tue, 07 Feb 2012 14:16:19 +0000

bytemark-symbiosis (2012:0124) stable; urgency=low

  * Updated bytemark-symbiosis package to depend on versions in squeeze-rc1
    release (i.e. 2012:0124).
  * Updated dependencies for bytemark-symbiosis not to have a weak-misc-
    depends.

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Tue, 24 Jan 2012 13:39:09 +0000

bytemark-symbiosis (2012:0117) stable; urgency=low

  * Removes debian-volatile distro from sources.lists if found.
  * Now adds to the MOTD rather than overwriting it.
  * Versioned all symbiosis dependencies to force upgrades.

 -- Patrick J Cherry <patrick@bytemark.co.uk>  Tue, 17 Jan 2012 17:22:19 +0000

bytemark-symbiosis (2011:1209) stable; urgency=low

  * Updated standards version (no change).

 -- Steve Kemp <steve@bytemark.co.uk>  Fri, 09 Dec 2011 15:43:43 +0000

bytemark-symbiosis (2010:0817) stable; urgency=low

  * Remove the old bytemark-vhost init script.
  * Changed "metapackage" & "meta package" to "meta-package" in our description.

 -- Steve Kemp <steve@bytemark.co.uk>  Tue, 17 Aug 2010 10:11:12 +0000

bytemark-symbiosis (2010:0519) stable; urgency=low

  [ Steve Kemp ]
  * Renamed the package.

  [ Patrick J Cherry ]
  * Switched to dpkg-source 3.0 (native) format

 -- Steve Kemp <steve@bytemark.co.uk>  Thu, 03 Jun 2010 13:51:31 +0100

bytemark-vhost (2009:1029-1) stable; urgency=low

  * Updated /etc/motd to have mention of the Bytemark Vhost system.
  * Added an init-script to report the use of vhost.

 -- Steve Kemp <steve@bytemark.co.uk>  Thu, 29 Oct 2009 10:00:01 +0000

bytemark-vhost (20090707153244) stable; urgency=low

  * Per-Lenny vhost repository, rather than branches

 -- Steve Kemp <steve@bytemark.co.uk>  Tue, 7 Jul 2009 15:32:44 +0000

bytemark-vhost (20090522105210) stable; urgency=low

  * New release for Lenny.

 -- Steve Kemp <steve@bytemark.co.uk>  Fri, 22 May 2009 10:52:10 +0000

bytemark-vhost (20081117163642) stable; urgency=low

  * Updated the short description of this package.

 -- Steve Kemp <steve@bytemark.co.uk>  Mon, 17 Nov 2008 16:36:16 +0000

bytemark-vhost (20081110153347) stable; urgency=low

  * No longer rely upon the bytemark-vhost-ssh-protection package.

 -- Steve Kemp <steve@bytemark.co.uk>  Fri, 11 Nov 2008 16:47:32 +0000

bytemark-vhost (20081110153346) stable; urgency=low

  * Updated all dependencies to be in order.

 -- Steve Kemp <steve@bytemark.co.uk>  Tue, 11 Nov 2008 13:44:31 +0000

bytemark-vhost (20081110153344) stable; urgency=low

  * The Bytemark Virtual Hosting Package bytemark-vhost
    - Support may be found at http://vhost.bytemark.co.uk/

 -- Steve Kemp <steve@bytemark.co.uk>  Mon, 10 Nov 2008 15:33:44 +0000
