#!/bin/bash

set -e

if [ "x$1" == "x" ]; then
  echo "Error: I need a repo name, such as 'stretch-testing'" > /dev/stderr
  exit 1
fi

REPO=$1

head -n 1 ./*/debian/changelog | grep '^sympl' | sed -e 's|(||' -e 's|).*||' | sort > /tmp/local_versions

# get the list of Sympl packages from the repo

arch="$( uname -r | sed 's|.*-||')"

# try new structure, failover to old otherwise
wget -q -O - "http://packages.mythic-beasts.com/mythic/dists/$REPO/main/binary-$arch/Packages" > /tmp/packages_repo || wget -q -O - "http://packages.mythic-beasts.com/mythic/dists/$REPO/main/source/Sources.xz" | unxz > /tmp/packages_repo
 
cat /tmp/packages_repo \
  | sed -n '/^Package: /,/^Version: /p' \
  | grep '^Package: \|^Version: ' \
  | sed -e 's|^Package: |\t|' -e 's|^Version: ||' \
  | tr '\n' ' ' \
  | tr '\t' '\n' \
  | grep '^sympl' \
  | sort -r \
  > /tmp/packages

if [ $( cat /tmp/packages | wc -l ) -lt 13 ]; then
  echo "ERROR: Repo appears to contain less than 13 Sympl Packages, something probably went wrong - FAIL!"
  exit 1
fi

cp /tmp/local_versions /tmp/from_repo

cat /tmp/local_versions | while read PACKAGE LOCAL_VER ; do
#  echo Checking for previous version of $PACKAGE $LOCAL_VER
  if grep "^$PACKAGE " /tmp/packages > /tmp/check-package; then
    LOCAL_VER_NUM="$( echo $LOCAL_VER | tr -d '.' )"
    cat /tmp/check-package | while read skip REPO_VER ; do
      REPO_VER_NUM="$( echo $REPO_VER | tr -d '.' )"
      if [ $REPO_VER_NUM -lt $LOCAL_VER_NUM ]; then
        sed -i "s|^$PACKAGE .*|$PACKAGE $REPO_VER|" /tmp/from_repo
        break
    #  else
    #    echo skipping $REPO_VER
      fi
    done
  else
    sed -i "s|^$PACKAGE .*||" /tmp/from_repo
 #   echo new package: $PACKAGE
  fi
done

rm /tmp/check-package /tmp/packages

echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

apt-get -qq update
apt-get -qq -y upgrade

wget -q https://mirror.mythic-beasts.com/mythic/support@mythic-beasts.com.gpg.key -O /etc/apt/trusted.gpg.d/support@mythic-beasts.com.asc

echo deb http://packages.mythic-beasts.com/mythic/ $REPO main > /etc/apt/sources.list.d/sympl_mythic-beasts.list

cp -r repo/ /
chmod -R 664 /repo ; chmod -R +X /repo
echo "deb [trusted=yes] file:/repo local main" > /etc/apt/sources.list.d/local.list

apt-get -qq update

echo ---------- Installing -----------
cat /tmp/from_repo | tr ' ' '\t'
echo ---------------------------------

apt-get -q -y install --allow-unauthenticated --install-recommends $( cat /tmp/from_repo | tr ' ' '=' | tr '\n' ' ' )

rm /etc/apt/sources.list.d/sympl_mythic-beasts.list

apt-get -qq update

echo ----------- Upgrading -----------
diff /tmp/from_repo /tmp/local_versions | grep '^> ' | sed -e 's|> ||' | tr ' ' '\t'
echo ---------------------------------

apt-get -q -y install --allow-unauthenticated --install-recommends $( diff /tmp/from_repo /tmp/local_versions | grep '^>' | sed -e 's|> ||' | tr ' ' '=' | tr '\n' ' ' )

apt-get -y autoremove

dpkg -l 'sympl-*' | grep '^ii' | awk '{ print $2 " " $3 }'

exit 0

