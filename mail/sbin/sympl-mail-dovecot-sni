#!/bin/bash

set -e

if [ $( find -L /srv/*/config/ssl/current/ -maxdepth 1 -mindepth 1 -name 'ssl.crt' -print | wc -l ) -eq 0 ]; then
  # No certs available, so check if /etc/dovecot/sympl.d/10-main/60-sni exists
  if [ -f /etc/dovecot/sympl.d/10-main/60-sni ]; then
    # it exists, so remove it
    rm /etc/dovecot/sympl.d/10-main/60-sni
    # then rebuild the configuration if theres a Makefile (ie: sympl-mail is installed)
    if [ -f /etc/dovecot/Makefile ]; then
      cd /etc/dovecot
      sudo /usr/bin/make test
      sudo /usr/bin/make > /dev/null
      sudo /usr/sbin/service dovecot reload > /dev/null 2>&1 | true
    fi
    exit 0
  else
    # Nothing to do yet, so just exit.
    exit 0
  fi
fi

for certificate in $( find -L /srv/*/config/ssl/current/ -maxdepth 1 -mindepth 1 -name 'ssl.crt' -print); do
  certpath="$( echo $certificate | sed 's|/config/ssl/current/.*$|/config/ssl/current|' )"
  # Ensure there is a matching key file, and the path doesnt include an underscore
  if [ -f "${certpath}/ssl.key" ] && [ -f "${certpath}/ssl.combined" ] && [ "$certpath" != "*_*" ] ; then
    # Go through the certs, listing all the domains, and filter them, one cert per domain.
    openssl x509 -noout -text -in "$certificate" \
    | grep 'Subject: CN\|DNS:' \
    | sed -e 's|, DNS:|\n|g' -e 's|DNS:|\n|g' -e 's|.*Subject: CN = ||' \
    | grep ^[a-zA-Z0-9] \
    | sort | uniq \
    | while read domain; do echo "$certpath $certificate $domain"; done
  fi
done | sort -k 3,3 | uniq -f 2 > /dev/shm/sympl-mail-dovecot-sni.data

# Write the config snippet to /dev/shm
echo "# Auto generated SNI configuration by sympl-mail-dovecot-sni." > /dev/shm/sympl-mail-dovecot-sni.config
cat /dev/shm/sympl-mail-dovecot-sni.data | while read certpath certificate domain; do
  echo "# Enable SNI for $domain"
  echo "local_name $domain {"
  echo "  ssl_cert = <$certpath/ssl.combined"
  echo "  ssl_key  = <$certpath/ssl.key"
  echo "}"
  echo
done >> /dev/shm/sympl-mail-dovecot-sni.config

# Compare it with what's there already.
if [ -f "/etc/dovecot/sympl.d/10-main/60-sni" ]; then
  if diff /dev/shm/sympl-mail-dovecot-sni.config /etc/dovecot/sympl.d/10-main/60-sni > /dev/null ; then
    # Config hasnt changed, so just exit.
    exit 0
  fi
fi

# Move the new config into place...
mv /dev/shm/sympl-mail-dovecot-sni.config /etc/dovecot/sympl.d/10-main/60-sni

# ... and build the new config if the Makefile is in place.
if [ -f /etc/dovecot/Makefile ]; then
  cd /etc/dovecot
  sudo /usr/bin/make test
  sudo /usr/bin/make > /dev/null
  sudo /usr/sbin/service dovecot reload > /dev/null 2>&1 | true
fi

if [ -f /dev/shm/sympl-mail-dovecot-sni.data ]; then rm /dev/shm/sympl-mail-dovecot-sni.data; fi

exit 0
